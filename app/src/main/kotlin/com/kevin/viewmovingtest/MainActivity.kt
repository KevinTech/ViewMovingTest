package com.kevin.viewmovingtest

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.animation.AnimationUtils
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private lateinit var objectAnimator: ObjectAnimator
    private lateinit var valueAnimator: ValueAnimator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_value_animation.setOnClickListener { valueSimpleTest() }
        btn_object_animation.setOnClickListener { objectSimpleTest() }
        btn_scroller.setOnClickListener { scrollerAnimation() }
        btn_animation_set.setOnClickListener { animationSet() }
        btn_animation_xml.setOnClickListener { moveViaAnimation() }
    }

    private fun animationSet() {
        val animator1 = ObjectAnimator.ofFloat(tv_test, "translationX", 0.0f, 200.0f, 0f)
        val animator2 = ObjectAnimator.ofFloat(tv_test, "scaleX", 1.0f, 2.0f)
        val animator3 = ObjectAnimator.ofFloat(tv_test, "rotationX", 0.0f, 90.0f, 0.0f)
        val set = AnimatorSet()
        set.duration = 1000
        set.play(animator1).with(animator2).after(animator3)
        set.start()
    }

    private fun scrollerAnimation() {
//        moveViaAnimation()
        // 移动cl中所有view
        mv_test.smoothScrollTo(-900, -200)
    }

    private fun moveViaAnimation() {
        // 使用动画使其位移
        mv_test.animation = AnimationUtils.loadAnimation(this, R.anim.translate)
        ObjectAnimator.ofFloat(mv_test, "translationX", 0.toFloat(), 300.toFloat()).setDuration(1000).start()
    }

    private fun objectSimpleTest() {
        objectAnimator = ObjectAnimator.ofFloat(mv_test, "translationX", 200.toFloat())
        objectAnimator.duration = 3000
        objectAnimator.start()
    }

    private fun valueSimpleTest() {
        valueAnimator = ValueAnimator.ofInt(0, 400)
        valueAnimator.duration = 3000
        valueAnimator.addUpdateListener { valueAnimator ->
            val currentValue: Int = valueAnimator.animatedValue as Int
            tv_test.layout(tv_test.left, currentValue, tv_test.right, currentValue + tv_test.height)
        }
        valueAnimator.start()
    }
}

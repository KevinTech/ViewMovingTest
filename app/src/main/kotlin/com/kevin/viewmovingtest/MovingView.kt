package com.kevin.viewmovingtest

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.widget.Scroller


/**
 * Created by Kobe on 2017/10/11.
 * Moving view
 */

class MovingView : View {

    companion object {
        private val TAG = "MovingView"
    }

    private lateinit var mScroller: Scroller

    // 记录点击时的坐标
    private var lastX = 0
    private var lastY = 0

    // 构造函数
    constructor(context: Context) : super(context) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context)
    }

    // 触碰监听
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        event?.let {
            // 获取当前坐标
            val currentX: Int = event.x.toInt()
            val currentY: Int = event.y.toInt()
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    Log.d(TAG, "MotionEvent.ACTION_DOWN")
                    lastX = currentX
                    lastY = currentY
                }
                MotionEvent.ACTION_MOVE -> {
                    Log.d(TAG, "MotionEvent.ACTION_MOVE")
                    // 移动距离
                    Log.d(TAG, "Left:$left,Right:$right,Top:$top,Bottom:$bottom")
                    val offsetX = currentX - lastX
                    val offsetY = currentY - lastY
                    // 设置View的坐标

//                     使用layout()
                    layout(left + offsetX, top + offsetY, right + offsetX, bottom + offsetY)

                    // 使用LayoutParams
//                    useLayoutParams(offsetX, offsetY)

                    // 使用offsetLeftAndRight()和offsetTopAndBottom()
//                    offsetLeftAndRight(offsetX)
//                    offsetTopAndBottom(offsetY)

                    // 使用scrollTo与scrollBy
//                    (parent as View).scrollBy(-offsetX, -offsetY)

                }
            }
        }
        return true
    }

    private fun useLayoutParams(offsetX: Int, offsetY: Int) {
        val lp: ConstraintLayout.LayoutParams = layoutParams as ConstraintLayout.LayoutParams
        lp.leftMargin = offsetX + left
        lp.topMargin = offsetY + top
        layoutParams = lp
    }

    private fun init(context: Context) {
        // 使用Scroller
        mScroller = Scroller(context)
    }

    override fun computeScroll() {
        super.computeScroll()
        if (mScroller.computeScrollOffset()) {
            (parent as View).scrollTo(mScroller.currX, mScroller.currY)
            //通过不断的重绘不断的调用computeScroll方法
            invalidate()
        }
    }

    fun smoothScrollTo(destX: Int, destY: Int) {
        val scrollX = scrollX
        val scrollY = scrollY
        mScroller.startScroll(scrollX, scrollY, destX, destY, 6000)
        invalidate()
    }
}
